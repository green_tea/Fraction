/*


template <typename T>
Fraction<T>::Fraction(T num, T den = 1) {
    long long integerNumPart = trunc(num);
    long double doubleNumPart = num - integerNumPart;
    long long doubleNumLen = 1;
    while (doubleNumPart > trunc(doubleNumPart)) {
        doubleNumPart *= 10;
        doubleNumLen *= 10;
    }
    Fraction<T>::num = integerNumPart * doubleNumLen + trunc(doubleNumPart);
    Fraction<T>::den = doubleNumLen * den;
    Fraction<T>::reduction();
}

template <typename T>
Fraction<T>::Fraction() {}

template <typename T>
Fraction<T>& Fraction<T>::operator=(const Fraction<T>& rhs) {
    if (this == &rhs) return *this;
    return *this = rhs;
}

template <typename T>
const T Fraction<T>::denomerator() const {
    return Fraction<T>::den;
}

template <typename T>
const T Fraction<T>::numerator() const {
    return Fraction<T>::num;
}

template <typename T>
T Fraction<T>::gcd(T a, T b) {
    if (a == 0) return b;
    return Fraction<T>::gcd(b % a, a);
}

template <typename T>
void Fraction<T>::reduction() {
    T k = Fraction<T>::gcd(Fraction<T>::num, Fraction<T>::den);
    Fraction<T>::num /= k;
    Fraction<T>::den /= k;
}

template <typename T>
const Fraction<T> operator-(const Fraction<T>& fraction) {
    return Fraction<T>(-fraction.num, fraction.den);
}

template <typename T>
const Fraction<T> operator+(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    Fraction<T> answer;
    answer.den = lhs.den * rhs.den;
    answer.num = lhs.num * rhs.den + rhs.num * lhs.den;
    answer.reduction();
    return answer;
}

template <typename T>
const Fraction<T> operator-(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    return lhs + (-rhs);
}

template <typename T>
const Fraction<T> operator*(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    Fraction<T> ans(lhs.num * rhs.num, lhs.den * rhs.den);
    ans.reduction();
    return ans;
}

template <typename T>
const Fraction<T> operator/(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    Fraction<T> ans(lhs.num * rhs.den, lhs.den * rhs.num);
    ans.reduction();
    return ans;
}

template <typename T>
Fraction<T>& operator+=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = lhs + rhs;
    return lhs;
}

template <typename T>
Fraction<T>& operator-=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = rhs - lhs;
    return lhs;
}

template <typename T>
Fraction<T>& operator*=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = lhs * rhs;
    return lhs;
}

template <typename T>
Fraction<T>& operator/=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = lhs / rhs;
    return lhs;
}

template <typename T>
const bool operator<(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    return lhs.num * rhs.den < rhs.num * lhs.den;
}

template <typename T>
const bool operator>(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    return !(lhs < rhs);
}

template <typename T>
const bool operator==(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    return (lhs.num == rhs.num) && (lhs.den == rhs.den);
}

template <typename T>
const bool operator<=(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    return (lhs < rhs) || (lhs == rhs);
}

template <typename T>
const bool operator>=(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    return (lhs > rhs) || (lhs == rhs);
}

template <typename T>
const bool operator!=(const Fraction<T>& lhs, const Fraction<T>& rhs) {
    return !(lhs == rhs);
}

template <typename T>
std::istream& operator>>(std::istream& input, Fraction<T>& fraction) {
    input >> fraction.num >> fraction.den;
    return input;
}

template <typename T>
std::ostream& operator<<(std::ostream& output, const Fraction<T>& fraction) {
    output << fraction.num << "/" << fraction.den;
    return output;
}*/