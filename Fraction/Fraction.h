#pragma once
#include "stdafx.h"
#include "Fraction.h"
#include <iostream>
#include <cmath>
#include <iomanip>
#include <stdexcept>

template <typename T>
class Fraction {
protected:
    T num = 0, den = 1;

    T Fraction<T>::gcd(T a, T b) {
        if (a == 0) return b;
        return Fraction<T>::gcd(b % a, a);
    }

    void reduction() {
        T k = gcd(num, den);
        num /= k;
        den /= k;
        if (den < 0) {
            num = -num;
            den = -den;
        }
    }

public:

    template <typename H>
    Fraction(H num, H den = 1) {
        long long integerNumPart = trunc(num);
        long double doubleNumPart = num - integerNumPart;
        long long doubleNumLen = 1;
        while (doubleNumPart > trunc(doubleNumPart)) {
            doubleNumPart *= 10;
            doubleNumLen *= 10;
        }
        this->num = integerNumPart * doubleNumLen + trunc(doubleNumPart);
        this->den = doubleNumLen * den;
        reduction();
    }

    Fraction() {}

    Fraction<T>& operator=(const Fraction<T>& rhs) {
        if (this == &rhs) return *this;
        num = rhs.num;
        den = rhs.den;
        return *this;
    }

    const T denomerator() const {
        return den;
    }

    const T numerator() const {
        return num;
    }


    friend const Fraction<T> operator-(const Fraction<T>& fraction) {
        return Fraction<T>(-fraction.num, fraction.den);
    }

    friend const Fraction<T> operator+(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        Fraction<T> answer;
        answer.den = lhs.den * rhs.den;
        answer.num = lhs.num * rhs.den + rhs.num * lhs.den;
        answer.reduction();
        return answer;
    }

    friend const Fraction<T> operator-(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        return lhs + (-rhs);
    }

    friend const Fraction<T> operator*(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        Fraction<T> ans(lhs.num * rhs.num, lhs.den * rhs.den);
        ans.reduction();
        return ans;
    }

    friend const Fraction<T> operator/(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        Fraction<T> ans(lhs.num * rhs.den, lhs.den * rhs.num);
        ans.reduction();
        return ans;
    }

    friend const bool operator>(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        return !(lhs < rhs);
    }

    friend const bool operator==(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        return (lhs.num == rhs.num) && (lhs.den == rhs.den);
    }

    friend const bool operator<=(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        return (lhs < rhs) || (lhs == rhs);
    }

    friend const bool operator>=(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        return (lhs > rhs) || (lhs == rhs);
    }

    friend const bool operator!=(const Fraction<T>& lhs, const Fraction<T>& rhs) {
        return !(lhs == rhs);
    }

    friend std::istream& operator >> (std::istream& input, Fraction<T>& fraction) {
        input >> fraction.num >> fraction.den;
        return input;
    }

    friend std::ostream& operator<<(std::ostream& output, const Fraction<T>& fraction) {
        if (fraction.den == 1) cout << fraction.num;
        else output << fraction.num << "/" << fraction.den;
        return output;
    }
};

template <typename T>
Fraction<T>& operator*=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = lhs * rhs;
    return lhs;
}

template <typename T>
Fraction<T>& operator/=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = lhs / rhs;
    return lhs;
}

template <typename T>
Fraction<T>& operator+=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = lhs + rhs;
    return lhs;
}

template <typename T>
Fraction<T>& operator-=(Fraction<T>& lhs, const Fraction<T>& rhs) {
    lhs = rhs - lhs;
    return lhs;
}
